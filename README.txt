
Description
===========
This module allows you to set meta tags for each Drupal page.

It based on Context module so it is very flexible. You can specify meta tags
for node page by type, taxonomy term page or any other path.

Not including the settings page, Context Metatags don't have its own interface.
It uses Context User Interface to setup meta tags.

The following meta tags are now abailable:
- title
- description
- keywords
- robots

Context Metatags provides some filters that allows you, for instance, to
limit meta tags to only 150 characters or lowercase keywords filter.

Support for Token module is also available.

I think, Context Metatags is a good and lightweight alternative instead
Nodewords, Integrated Metatags and Page Ttle. Especialy for drupalers who
likes use Context in diffrent ways.

The formula that describe this module:
Context Metatags = Nodewords + Integrated Metatags + Page Title

Context Metatags works only with Context 3.
