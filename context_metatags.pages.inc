<?php

/**
 * @file
 * Metatags administration and module settings UI.
 */

/**
 * Settings form builder callback.
 */
function context_metatags_settings() {
  $form = array(
    'context_metatags_settings' => array('#tree' => TRUE)
  );
  $form['context_metatags_settings']['title_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Title length'),
    '#description' => t(
      'Set a character limit on %tag metatag. 0 means no limit.',
      array('%tag' => 'title')),
    '#default_value' => context_metatags_settings_get('title_max_length'),
  );
  $form['context_metatags_settings']['description_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Description length'),
    '#description' => t(
      'Set a character limit on %tag metatag. 0 means no limit.',
      array('%tag' => 'description')),
    '#default_value' => context_metatags_settings_get('description_max_length'),
  );
  $form['context_metatags_settings']['keywords_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Keywords length'),
    '#description' => t(
      'Set a character limit on %tag metatag. 0 means no limit.',
      array('%tag' => 'keywords')),
    '#default_value' => context_metatags_settings_get('keywords_max_length'),
  );
  $form['context_metatags_settings']['ignore_error_pages'] = array(
    '#type' => 'checkbox',
    '#title' => 'Ignore error pages',
    '#description' => t('If enabled, context_metatags module will ignore error pages. This is usually good thing.'),
    '#default_value' => context_metatags_settings_get('ignore_error_pages'),
  );
  $form['context_metatags_settings']['lowercase_keywords'] = array(
    '#type' => 'checkbox',
    '#title' => 'Lowercase keywords',
    '#description' => 'Use drupal_strtolower() function before display keyowords.',
    '#default_value' => context_metatags_settings_get('lowercase_keywords')
  );

  return system_settings_form($form);
}
