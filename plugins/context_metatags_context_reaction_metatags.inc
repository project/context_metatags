<?php

/**
 * @file
 * Define reaction that allows users to define metatags per context.
 */

/**
 * Expose themes as context reactions.
 */
class context_metatags_context_reaction_metatags extends context_reaction {

  /**
   * Allow admins to provide a adblock namespace.
   */
  function options_form($context) {
    $values = $this->fetch_from_context($context);

    $form = array();
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($values['title']) ? $values['title'] : '',
      '#maxlength' => 255,
    );
    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => isset($values['description']) ? $values['description'] : '',
      '#resizable' => TRUE,
      '#rows' => 3,
    );
    $form['keywords'] = array(
      '#type' => 'textarea',
      '#title' => t('Keywords'),
      '#default_value' => isset($values['keywords']) ? $values['keywords'] : '',
      '#resizable' => TRUE,
      '#rows' => 3,
    );
    $form['robots'] = array(
      '#type' => 'textfield',
      '#title' => t('Robots'),
      '#maxlength' => 20,
      '#default_value' => isset($values['robots']) ? $values['robots'] : '',
    );

    if (module_exists('token')) {
      $form['token_help'] = array(
        '#title' => t('Replacement patterns'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => FALSE,
        'help' => array('#value' => theme('token_help'))
      );
    }

    return $form;
  }

  function execute() {
    $contexts = context_active_contexts();
    $classes = array();
  }
}
